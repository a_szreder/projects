/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pharmacyhelper;

/**
 *
 * @author Piotr
 */
public enum Option {

    ADD("Add"), REMOVE("Remove"), EXPIRED("Expired"),SEARCH ("Search"),NAME ("Name"),TYPE ("Type"),UNDEFINED ("?");
    String optionString;

    private Option(String option) {
        optionString = option;
    }

    public String getString() {
        return optionString;
    }
}
