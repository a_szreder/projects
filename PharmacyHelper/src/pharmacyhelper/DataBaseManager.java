/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pharmacyhelper;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.bson.types.ObjectId;

/**
 *
 * @author Piotr
 */
public class DataBaseManager {

    MongoClient mongo;
    private DB db;
    private DBCollection medicaments;

    public DataBaseManager() {
        mongo = new MongoClient("localhost", 3979);
        db = mongo.getDB("Pharmaceuticals");
        medicaments = db.getCollection("medicaments");

//        addNewMedicament(antifebrile);
        getDataBases();

    }

    private List<String> getDataBases() {

        if (mongo == null) {
            System.out.println("Nie udana próba połączenia z bazą danych");
            return null;
        }

        List<String> dbs = mongo.getDatabaseNames();
        for (String db : dbs) {
            System.out.println(db);
        }
        return dbs;
    }

    private void addNewMedicament(DBCollection collection) {

        System.out.println("JSON parse example...");

        String medicAntifebrile = "{'database': 'Pharmaceuticals','collection': 'antifebrile',"
                + "'medicaments': {'medicament1': {'name': 'Polopiryna S','quantity': '2','expireData': ['04.2017','10.2017']},"
                + "'medicament2': {'name': 'Gripex max','quantity': '1','expiredData': ['05.2018']}}}";
//        String medic = "{'database' : 'Pharmaceuticals','name' : 'Gripex max','type':'antifebrile','quantity':'1',"
//                + "'data':{'value':'05.2018'}"
//                + "'other' : [{'add' : 'New','onclick':'AddNewData()'},"
//                + "{'remove':'Old','onclick':'RemoveOldData()'},"
//                + "{'alert':'Expired','onclick':ShowExpiredMedicamentWithData()'}]}}";

        DBObject dbObject = (DBObject) JSON.parse(medicAntifebrile);

        collection.insert(dbObject);

        DBCursor cursorDocJSON = collection.find();
        while (cursorDocJSON.hasNext()) {
            System.out.println(cursorDocJSON.next());
        }
    }

    public void addNewMedicament(String nameOfMedical, long time, String type) {

        addNewMedicament(nameOfMedical, time, type, medicaments);
    }

    private void addNewMedicament(String nameOfMedical, long time, String type, DBCollection medicaments) {
        BasicDBObject document = new BasicDBObject();
        document.put("name", nameOfMedical);
        document.put("expiredData", time);
        document.put("type", type);
        medicaments.insert(document);

        DBCursor cursorDocJSON = medicaments.find();
        while (cursorDocJSON.hasNext()) {
            System.out.println(cursorDocJSON.next());
        }
    }

    public JTable removeOldMedicament(String nameOfMedical, long time, String type) {

        JTable table;
        table = removeOldMedicament(nameOfMedical, time, type, medicaments, db);
//        System.out.println(toRemove);
        return table;
    }

    private JTable removeOldMedicament(String nameOfMedical, long time, String type, DBCollection medicaments, DB db) {

        JTable table;

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", nameOfMedical);
//        toRemove.insert(searchQuery);
        searchQuery.put("expiredData", time);
//        toRemove.insert(searchQuery);
        searchQuery.put("type", type);

        DBCursor toRemove = medicaments.find(searchQuery);
//        while (toRemove.hasNext()) {
//            System.out.println(toRemove.next());
//        }

        table = createTable(toRemove);
        return table;
    }

    public JTable removeOldMedicament(String name, String type) {

        JTable table;
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", name);
        searchQuery.put("type", type);

        DBCursor toRemove = medicaments.find(searchQuery);
//        while (toRemove.hasNext()) {
//            System.out.println(toRemove.next());
//        }
        table = createTable(toRemove);
        return table;
    }

    public void showExpiredMedicament(long time) {
//        getTodayDate();
        searchingExpiredMedicaments(time, medicaments);
    }

    private void searchingExpiredMedicaments(long time, DBCollection medicaments) {
        long today = System.currentTimeMillis();
        DBCursor cursor;
        List<String> expiredMedicaments = new ArrayList<>();

    }

    JTable searchByName(String nameOfMedical) {

        JTable table;

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", nameOfMedical);
        DBCursor foundMedicaments = medicaments.find(searchQuery);

//        while (foundMedicaments.hasNext()) {
//            System.out.println(foundMedicaments.next());
//        }
        table = createTable(foundMedicaments);

        return table;
    }

    JTable searchingByType(String typeString) {

        JTable table;

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("type", typeString);
        DBCursor foundMedicaments = medicaments.find(searchQuery);
//        foundMedicaments.addOption(Bytes.QUERYOPTION_NOTIMEOUT);

//        while (foundMedicaments.hasNext()) {
//            System.out.println(foundMedicaments.next());
//        }
        table = createTable(foundMedicaments);

        return table;
    }

    public void searchingMedicament(String nameOfMedical, long time, String type) {

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", nameOfMedical);
        searchQuery.put("expiredData", time);
        searchQuery.put("type", type);

        DBCursor searched = medicaments.find(searchQuery);

//        while (searched.hasNext()) {
//            System.out.println(searched.next());
//        }
    }

    private static Option getOption(String stringOp) {
        Option op = Option.UNDEFINED;
        for (Option option : Option.values()) {
            if (option.getString().equals(stringOp)) {
                op = option;
            }
        }
        return op;

    }

    private Date getTodayDate() {
        Date date = null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        System.out.println(year + " " + month + " " + day);
        return date;
    }

    private long toTimestamp(Date date) {
        long time = 0;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return time;
    }

    private JTable createTable(DBCursor medicals) {
        JTable table = null;

        String[] columnNames = {"id", "Name", "Type", "Expired Data"};

        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        while (medicals.hasNext()) {
            DBObject obj = medicals.next();
            String name = (String) obj.get("name");
            String type = (String) obj.get("type");
            long expiredData = Long.valueOf(obj.get("expiredData").toString());
            ObjectId id = (ObjectId) obj.get("_id");
            model.addRow(new Object[]{id, name, type, expiredData});
        }
        table = new JTable(model);

        medicals.close();

        return table;
    }

    void remove(ObjectId id) {
//        System.out.println(id);
        BasicDBObject doc = new BasicDBObject();
        doc.put("_id", id);
        System.out.println(doc);
        medicaments.remove(doc);
    
    }

}
