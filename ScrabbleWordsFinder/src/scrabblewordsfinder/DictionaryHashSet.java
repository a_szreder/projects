package scrabblewordsfinder;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author Piotr
 */
public class DictionaryHashSet extends HashSet<String> {

    public DictionaryHashSet(Collection<? extends String> clctn) {
        super(clctn);
    }

    public DictionaryHashSet() {
        super();
    }

    public boolean add(String word) {
        return super.add(word);
    }

    public boolean remove(String word) {
        return super.remove(word);
    }

    public boolean isWordInDictionary(String word) {
        return false;
    }

    public String getLongestWordWithGivenCharacters(char[] letters) {
        return null;
    }

    
}
