package scrabblewordsfinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Piotr
 */
public class WordFinder {

    private final DictionaryHashSet wordHashSet;
    private final static Logger LOGGER = Logger.getLogger(WordFinder.class.getName());

    public WordFinder() {
        wordHashSet = prepareDictionary();
    }

    public HashMap findBestWord(char[] givenLetters, char[] mustBeLetters) {
//        printDictionary(wordHashSet);
        DictionaryHashSet wantedHashSet = reduceDictionary(wordHashSet, givenLetters, mustBeLetters);
//        printDictionary(wantedHashSet);
        DictionaryHashSet finallyHashSet = findAcceptableWords(givenLetters, wantedHashSet, mustBeLetters);
//        printDictionary(finallyHashSet);
        HashMap wordWithValuesMap = getBestWordsHashMap(finallyHashSet);

        return wordWithValuesMap;
    }

    private DictionaryHashSet prepareDictionary() {

        DictionaryHashSet wordHashSet = new DictionaryHashSet();

        BufferedReader textBufferedReader = null;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream in = classLoader.getResourceAsStream("res/slowa.txt");
            textBufferedReader = new BufferedReader(new InputStreamReader(in));

            String line = textBufferedReader.readLine();

            while (line != null) {
                wordHashSet.add(line);

                line = textBufferedReader.readLine();
            }
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, "unable to open file with Dictionary", ex);
        } finally {
            if (textBufferedReader != null) {
                try {
                    textBufferedReader.close();
                } catch (IOException ex) {
                    LOGGER.log(Level.WARNING, "can't close Reader", ex);
                }
            }
        }

        return wordHashSet;
    }

    private DictionaryHashSet reduceDictionary(DictionaryHashSet fullDictionary, char[] givenLetters, char[] mustBeLetters) {

        DictionaryHashSet wantedHashSet = new DictionaryHashSet();

        for (String word : fullDictionary) {

            if (isWordValid(word, givenLetters, mustBeLetters)) {
                wantedHashSet.add(word);
            }
        }
        return wantedHashSet;
    }

    private boolean isWordValid(String word, char[] givenLetters, char[] mustBeLetters) {

        boolean containsMustBeLetters = false;
        char[] allLetters = splitTwoChar(givenLetters, mustBeLetters);
        String str = String.valueOf(mustBeLetters);
        if (word.length() <= allLetters.length) {

            if (word.contains(str)) {
                for (char i : allLetters) {
                    if (!containsLetters(i, word, allLetters)) {

                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    private void printDictionary(DictionaryHashSet wordHashSet) {

        System.out.println("ilość słów" + wordHashSet.size());
        for (String string : wordHashSet) {
            System.out.println(string);
        };

    }

    private boolean containsLetters(char i, String word, char[] allLetters) {

        int count = 0;
        for (char ch : word.toCharArray()) {
            if (ch == i) {
                count++;
            }
        }
        if (count > frequencyOccuranceOfLetter(i, allLetters)) {
            return false;
        } else {
            return true;

        }
    }

    private DictionaryHashSet findAcceptableWords(char[] givenLetters, DictionaryHashSet wantedHashSet, char[] mustBeLetters) {

        DictionaryHashSet finallyHashSet = new DictionaryHashSet(wantedHashSet);
        char[] allLetters = splitTwoChar(givenLetters, mustBeLetters);
        for (String word : wantedHashSet) {

            if (isThereWrongLetter(allLetters, convertWordToLetters(word)) == false) {
                finallyHashSet.remove(word);
            }
        }
        return finallyHashSet;
    }

    private boolean isThereWrongLetter(char[] allLetters, char[] lettersWord) {

        for (char i : lettersWord) {
            if (!containsLetters(i, allLetters)) {
                return false;
            }
        }
        return true;
    }

    private char[] convertWordToLetters(String word) {

        char[] lettersWord = new char[word.length()];
        for (int i = 0; i < lettersWord.length; i++) {
            lettersWord[i] = word.charAt(i);
        }
        return lettersWord;
    }

    private boolean containsLetters(char i, char[] allLetters) {

        for (char c : allLetters) {

            if (i == c) {
                return true;
            }
        }
        return false;
    }

    private HashMap getBestWordsHashMap(DictionaryHashSet finallyHashSet) {

        HashMap<String, Integer> wordsWithValuesMap = new HashMap<>();

        for (String word : finallyHashSet) {

            wordsWithValuesMap.put(word, letterValue(convertWordToLetters(word)));
        }
        int highestValue = findHighestWordValue(wordsWithValuesMap);
        System.out.println("Highest value is: " + highestValue);
        List<String> wordsToRemove = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : wordsWithValuesMap.entrySet()) {
            if (entry.getValue() < highestValue) {
                wordsToRemove.add(entry.getKey());
            }
        }
        for (String wordKeyToBeRemoved : wordsToRemove) {
            wordsWithValuesMap.remove(wordKeyToBeRemoved);
        }
        return wordsWithValuesMap;
    }

    private int findHighestWordValue(HashMap<String, Integer> wordsWithValuesMap) {
        Integer highestValue = null;
        for (Map.Entry<String, Integer> entry : wordsWithValuesMap.entrySet()) {
            if (highestValue == null) {
                highestValue = entry.getValue();
            } else {
                if (highestValue < entry.getValue()) {
                    highestValue = entry.getValue();
                }
            }
        }
        if (highestValue == null) {
            return 0;
        }
        return highestValue;
    }

    private int letterValue(char[] convertWordToLetters) {
        int totalValue = 0;

        for (char c : convertWordToLetters) {
            totalValue += LetterValue.getValue(c);
        }
        return totalValue;
    }

    private char[] splitTwoChar(char[] givenLetters, char[] mustBeLetters) {
        StringBuilder sb = new StringBuilder();
        sb.append(givenLetters);
        sb.append(mustBeLetters);
        char[] allLetters = sb.toString().toCharArray();

        return allLetters;

    }

    private int frequencyOccuranceOfLetter(char i, char[] allLetters) {
        int frequency = 0;

        for (char ch : allLetters) {
            if (ch == i) {
                frequency++;
            }
        }
        return frequency;
    }

}
